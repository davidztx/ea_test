# ea_test

This is a custom Drupal module for EnergyAustralia Coding Test.

## Demo
http://eacodingtestpaduxmn2x9.devcloud.acquia-sites.com

##Known issues

### 400 error
It will show: Service temporarily unavailable.

### Empty response
It will show: Car list is empty

### Show or model name is not set
It will show: missing_TYPE_TIMESTAMP