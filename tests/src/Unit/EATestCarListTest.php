<?php

namespace Drupal\Tests\ea_test\Unit;

use Drupal\ea_test\EATestCarList;
use Drupal\Tests\UnitTestCase;

/**
 * Test the Test Car list class
 *
 * @group ea_test
 */
class EATestCarListTest extends UnitTestCase {

  /**
   * Tests the Calculator::add() method.
   */

  public function testGetList() {

    $car_list = new EATestCarList();
    $car_list->setStringTranslation($this->getStringTranslationStub());

    $this->assertTRUE(!empty($car_list->getList()));
  }

}